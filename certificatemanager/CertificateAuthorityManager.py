import json
import os

from cryptography.fernet import Fernet
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric.dh import DHPrivateKey
from cryptography.hazmat.primitives.asymmetric.dsa import DSAPrivateKey
from cryptography.hazmat.primitives.asymmetric.ec import EllipticCurvePrivateKey
from cryptography.hazmat.primitives.asymmetric.rsa import RSAPrivateKey

from typing import Union

from crypto_helper import create_key

from CertificateOwner import CertificateOwnerInfo, CertificateOwner



class CertificateAuthority(CertificateOwner):
    @classmethod
    def load_from_encrypted_db(cls, db_filename, db_passphrase):
        with open(db_filename, "rb") as encrypted_db:
            salt = encrypted_db.read(16)

            if len(salt) < 16:
                return None

            key = create_key(
                salt,
                db_passphrase.encode("utf-8")
            )

            cipher = Fernet(key)
            decrypted_data = str(cipher.decrypt(encrypted_db.read()), encoding="utf-8")

            decrypted_key_storage = json.loads(decrypted_data)

            ca_needed_keys = ["magic_number", "private_key", "owner_info", "signed_servers"]
            if not all(key in decrypted_key_storage for key in ca_needed_keys):
                return None

            new_ca = cls(
                owner_info=decrypted_key_storage["owner_info"],
                private_key=serialization.load_pem_private_key(decrypted_key_storage["private_key"])
            )

            for server in decrypted_key_storage["singed_servers"]:
                new_ca.signed_certificates[server] = CertificateOwner.from_dict(
                    co_dict=decrypted_key_storage["singed_servers"][server],
                    ca=new_ca
                )

            return new_ca

    @classmethod
    def new_from_json(cls, json_file):
        with open(json_file, "r") as json_handle:
            json_data = json.load(json_handle)

            ca_info = CertificateOwnerInfo(**json_data["owner_info"])

            new_ca = cls(private_key=None, owner_info=ca_info)

            if "signed_certificates" in json_data:
                for identifier, certificate_data in json_data["signed_certificates"].items():
                    co_info = CertificateOwnerInfo(**certificate_data["owner_info"])
                    new_ca.add_signed_certificate(
                        identifier=identifier,
                        owner_info=co_info
                    )

            return new_ca

    def __init__(self,
                 owner_info: CertificateOwnerInfo,
                 private_key: Union(RSAPrivateKey, DSAPrivateKey, DHPrivateKey, EllipticCurvePrivateKey, None)):
        super().__init__(owner_info=owner_info, private_key=private_key)
        self.signed_certificates = dict()

    def update_from_json(self, json_file):
        with open(json_file, "r") as json_handle:
            json_data = json.load(json_handle)
            self.owner_info.update(**json_data["owner_info"])

            if "signed_servers" in json_data:
                for server_id, server_data in json_data["signed_servers"].items():
                    if server_id in self.signed_certificates:
                        self.signed_certificates[server_id].update_owner_info(**server_data["owner_info"])
                    else:
                        self.add_signed_certificate(
                            identifier=server_id,
                            owner_info=CertificateOwnerInfo(**server_data["owner_info"]),
                        )

    def add_signed_certificate(self,
                               identifier: str,
                               owner_info: CertificateOwnerInfo,
                               server_private_key: Union(RSAPrivateKey,
                                                    DSAPrivateKey,
                                                    DHPrivateKey,
                                                    EllipticCurvePrivateKey,
                                                    None) = None):
        if identifier in self.signed_certificates:
            return

        new_cert = CertificateOwner(
            owner_info=owner_info,
            private_key=server_private_key,
            certificate_authority=self)
        if new_cert:
            self.signed_certificates[identifier] = new_cert

    def store_to_db(self, db_filename, passphrase):
        with open(db_filename, "wb") as encrypted_db:
            salt = os.urandom(16)
            key = create_key(
                salt,
                passphrase.encode("utf-8")
            )

            decrypted_key_storage = dict()
            decrypted_key_storage["private_key"] = self.private_key.private_bytes(
                encoding=serialization.Encoding.PEM,
                format=serialization.PrivateFormat.TraditionalOpenSSL,
                encryption_algorithm=serialization.NoEncryption()
            ).decode("utf-8")
            decrypted_key_storage["owner_info"] = self.owner_info
            decrypted_key_storage["signed_servers"] = dict()
            for server in self.signed_certificates:
                decrypted_key_storage["signed_servers"][server] = self.signed_certificates[server].to_dict()

            cipher = Fernet(key)
            encrypted_data = cipher.encrypt((json.dumps(decrypted_key_storage, indent=4)).encode("utf-8"))
            encrypted_db.write(encrypted_data)

    def get_common_name(self):
        return self.owner_info["COMMON_NAME"]

