import base64
from datetime import datetime, timedelta

from cryptography import x509
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.primitives.asymmetric.dh import DHPrivateKey
from cryptography.hazmat.primitives.asymmetric.dsa import DSAPrivateKey
from cryptography.hazmat.primitives.asymmetric.ec import EllipticCurvePrivateKey
from cryptography.hazmat.primitives.asymmetric.rsa import RSAPrivateKey
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC


def generate_private_key():
    private_key = rsa.generate_private_key(
        public_exponent=65537, key_size=2048, backend=default_backend()
    )

    return private_key


def generate_public_certificate(private_key: RSAPrivateKey, oid_info: dict):
    attributes = list()
    for info in oid_info:
        oid_name = getattr(x509.oid.NameOID, info)
        if oid_name:
            attributes.append(x509.NameAttribute(oid_name, oid_info[info]))
    subject = x509.Name(attributes)

    # Because this is self signed, the issuer is always the subject
    issuer = subject

    # This certificate is valid from now until 30 days
    valid_from = datetime.utcnow()
    valid_to = valid_from + timedelta(days=30)

    # Used to build the certificate
    builder = (
        x509.CertificateBuilder()
        .subject_name(subject)
        .issuer_name(issuer)
        .public_key(private_key.public_key())
        .serial_number(x509.random_serial_number())
        .not_valid_before(valid_from)
        .not_valid_after(valid_to)
    )

    # Sign the certificate with the private key
    public_key = builder.sign(
        private_key, hashes.SHA256(), default_backend()
    )

    return public_key


def generate_csr(signing_key, oid_info: dict):
    attributes = list()
    subject_alt_names = []

    for info in oid_info:
        oid_name = getattr(x509.oid.NameOID, info)
        if oid_name:
            attributes.append(x509.NameAttribute(oid_name, oid_info[info]))
        elif info == "ALT_NAMES":
            for name in oid_info["ALT_NAMES"]:
                subject_alt_names.append(x509.DNSName(name))
    subject = x509.Name(attributes)
    san = x509.SubjectAlternativeName(subject_alt_names)

    builder = (
        x509.CertificateSigningRequestBuilder()
            .subject_name(subject)
            .add_extension(san, critical=False)
    )

    return builder.sign(signing_key, hashes.SHA256(), default_backend())


def sign_csr(csr,
             ca_name: str,
             ca_private_key:RSAPrivateKey):
    valid_from = datetime.utcnow()
    valid_until = valid_from + timedelta(days=30)
    builder = (
        x509.CertifacteBuilder()
        .subject_name(csr.subject)
        .issuer_name(ca_name)
        .public_key(csr.public_key())
        .serial_number(x509.random_serial_number())
        .not_valid_before(valid_from)
        .not_valid_after(valid_until)
    )

    for extension in csr.extensions:
        builder = builder.add_extension(extension.value, extension.critical)

    return builder.sign(
        private_key=ca_private_key,
        algorithm=hashes.SHA256(),
        backend=default_backend(),
    )


def create_key(salt, passphrase):
    kdf = PBKDF2HMAC(
        algorithm=hashes.SHA256(),
        length=32,
        salt=salt,
        iterations=100000,
    )
    key = base64.urlsafe_b64encode(kdf.derive(passphrase))
    return key
