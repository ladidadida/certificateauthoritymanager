from __future__ import annotations

from cryptography.hazmat.primitives import serialization

import crypto_helper as mycrypto

from cryptography.hazmat.primitives.asymmetric.dh import DHPrivateKey
from cryptography.hazmat.primitives.asymmetric.dsa import DSAPrivateKey
from cryptography.hazmat.primitives.asymmetric.ec import EllipticCurvePrivateKey
from cryptography.hazmat.primitives.asymmetric.rsa import RSAPrivateKey

from typing import Union

from CertificateOwnerInfo import CertificateOwnerInfo


class CertificateOwner:
    def __init__(self,
                 owner_info: CertificateOwnerInfo,
                 private_key: Union(RSAPrivateKey, DSAPrivateKey, DHPrivateKey, EllipticCurvePrivateKey, None),
                 certificate_authority: Union("CertificateOwner", None) = None):
        self.owner_info: CertificateOwnerInfo = owner_info
        self.certificate_authority = certificate_authority
        self.private_key = private_key

        if not self.private_key:
            self.generate_new_key()

    def set_certificate_authority(self, ca):
        self.certificate_authority = ca

    def to_dict(self):
        return {
            "private_key": self.private_key.private_bytes(
                encoding=serialization.Encoding.PEM,
                format=serialization.PrivateFormat.TraditionalOpenSSL,
                encryption_algorithm=serialization.NoEncryption()
            ).decode("utf-8"),
            "owner_info": self.owner_info
        }

    def generate_new_key(self):
        self.private_key = mycrypto.generate_private_key()

    def update_owner_info(self, owner_info: dict):
        if owner_info != self.owner_info:
            self.owner_info.update(owner_info)

    def export_private_key(self, filename: str, passphrase: str):
        with open(filename, "wb") as file:
            algorithm = serialization.BestAvailableEncryption(passphrase.encode("utf-8"))
            file.write(self.private_key.private_bytes(
                encoding=serialization.Encoding.PEM,
                format=serialization.PrivateFormat.TraditionalOpenSSL,
                encryption_algorithm=algorithm
            ))

    def export_public_certificate(self, filename: str):
        pub_cert = mycrypto.generate_public_certificate(self.private_key, self.owner_info)
        "s".encode()
        with open(filename, "wb") as file:
            file.write(pub_cert.public_bytes(encoding=serialization.Encoding.PEM))

    def export_signed_certificate(self, filename: str):
        if not self.certificate_authority:
            return

        with open(filename, "wb") as file:
            csr = mycrypto.generate_csr(signing_key=self.private_key, oid_info=self.owner_info)
            signed_cert = mycrypto.sign_csr(
                csr=csr,
                ca_name=self.certificate_authority.get_common_name(),
                ca_private_key=self.certificate_authority.private_key
            )
            file.write(signed_cert.public_bytes(encoding=serialization.Encoding.PEM))

    @classmethod
    def from_dict(cls, co_dict: dict, ca):
        if not co_dict or "private_key" not in co_dict or "owner_info" not in co_dict:
            return

        private_key = serialization.load_pem_private_key(co_dict["private_key"], None)
        owner_info = CertificateOwnerInfo()
        owner_info.update(co_dict["owner_info"])
        return cls(private_key, owner_info, ca)


