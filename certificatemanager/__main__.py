#!/usr/bin/python3
import click
from getpass import getpass

from CertificateAuthorityManager import CertificateAuthority


def print_version(ctx, param, value) -> None:
    if not value or ctx.resilient_parsing:
        return
    click.echo("CertificateAuthorityManager")
    click.echo("Version 0.0.1")
    ctx.exit()

@click.command()
@click.option("-v", "--version", is_flag=True, callback=print_version, expose_value=False, is_eager=True)
@click.option("-n", "--new", is_flag=True, help="Create a new database from scratch")
@click.option("-p", "--passphrase")
@click.option("-c", "--json-config", type=click.Path(), help="Create a new CA storage from config json file")
@click.option("-i", "--interactive", is_flag=True, help="Start interactive session")
@click.option("--export-ca-pub-key", type=click.Path("w", "utf-8"), is_flag=True, help="Export CA public key")
@click.option("--export-server-priv-key", type=(click.Path("w", "utf-8"), str), is_flag=True, help="Export Server private key")
@click.option("--export-server-pub-key", type=(click.Path("w", "utf-8"), str), is_flag=True, help="Export Server public key")
@click.argument("database", type=click.Path(exists=True))
def cli(version, new, passphrase, json_config, interactive, export_ca_pub_key, export_server_priv_key, export_server_pub_key, database):
    CA = None

    print(f"version: {version}")
    print(f"new: {new}")
    print(f"passphrase: {passphrase}")
    print(f"json_config: {json_config}")
    print(f"interactive: {interactive}")
    print(f"export_ca_pub_key: {export_ca_pub_key}")
    print(f"export_server_priv_key: {export_server_priv_key}")
    print(f"export_server_pub_key: {export_server_pub_key}")
    print(f"database: {database}")

    if new:
        if json_config:
            CA = CertificateAuthority.new_from_json(json_config)
        else:
            CA = CertificateAuthority.create_empty()
    elif interactive and database:
        if not passphrase:
            passphrase = getpass("Enter passphrase to load DB: ")
        CA = CertificateAuthority.load_from_encrypted_db(database, passphrase)
    else:
        pass

    if database and CA:
        if export_ca_pub_key:
            CA.export_ca_public_key(filename=export_ca_pub_key)
        if export_server_priv_key:
            server_priv_key_filename, server_priv_key_passphrase = export_server_priv_key
            CA.export_server_private_key(
                server_priv_key_filename,
                server_priv_key_passphrase
            )
        if export_server_pub_key:
            server_pub_key_filename, server_pub_key_passphrase = export_server_pub_key
            CA.export_server_public_signed_key(
                server_pub_key_filename,
                server_pub_key_passphrase
            )

    if not interactive:
        if database and CA:
            if not passphrase:
                passphrase = getpass("Enter passphrase to encrypt DB: ")
            CA.store_to_db(database, passphrase)
    else:
        # start command interpreter
        while True:
            command = input("> ")

            if command in ["h", "help", "?"]:
                print("Comming soon...")

            elif command == "print-ca-info":
                if CA:
                    print(CA.__dict__)
                else:
                    print("No CA created yet.")

            elif command == "create-new-ca":
                if CA:
                    print("An CA is already existing, shall it be overwritten?")
                    print("Note: All keys stored with the CA will be invalidated!")
                    reply = input(">> ")
                    if reply not in ["y", "Y", "Yes", "YES", "yes"]:
                        continue
                else:
                    print("For creation of a new CertificateAuthority certificate, a passphrase is needed.")
                    password = getpass()
                    CA = CertificateAuthority.create_empty(password)

            elif command == "add-server":
                if not CA:
                    print("There is no valid CA yet.")
                else:
                    print("A new server will be added. Please fill needed info.")
                    hostname = input("Hostename (e.g. 'localhost', '192.168.0.10'): ")
                    organization = input("Organization: ")
                    email = input("Email address: ")
                    country = input("Country (e.g. 'DE', 'US'): ")
                    state = input("State: ")
                    locality = input("Locality: ")
                    alt_names = input("Alt. Names (e.g. 'www.example.org de.example.org wiki.example.org'): ").split()
                    server_passphrase = getpass("Now you can sea password for the server's private key: ")
                    CA.add_server(
                        hostname=hostname,
                        organization=organization,
                        email_address=email,
                        country=country if country else "DE",
                        state=state,
                        alt_names=alt_names,
                        passphrase=server_passphrase,
                    )

            elif command == "store-db":
                if not database:
                    database = input("Please enter path where database file shall be stored: ")
                if not passphrase:
                    passphrase = getpass("Enter passphrase to encrypt DB: ")
                CA.store_to_db(database, passphrase)

            elif command == "exit":
                print("Will exit now. GOOD BYE!")
                break
            else:
                pass

cli()

