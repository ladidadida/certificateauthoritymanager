from typing import TypedDict, List


class CertificateOwnerInfo(TypedDict, total=False):
    COMMON_NAME: str
    COUNTRY_NAME: str
    LOCALITY_NAME: str
    STATE_OR_PROVINCE_NAME: str
    STREET_ADDRESS: str
    ORGANIZATION_NAME: str
    ORGANIZATIONAL_UNIT_NAME: str
    SERIAL_NUMBER: str
    SURNAME: str
    GIVEN_NAME: str
    TITLE: str
    GENERATION_QUALIFIER: str
    X500_UNIQUE_IDENTIFIER: str
    DN_QUALIFIER: str
    PSEUDONYM: str
    USER_ID: str
    DOMAIN_COMPONENT: str
    EMAIL_ADDRESS: str
    JURISDICTION_COUNTRY_NAME: str
    JURISDICTION_LOCALITY_NAME: str
    JURISDICTION_STATE_OR_PROVINCE_NAME: str
    BUSINESS_CATEGORY: str
    POSTAL_ADDRESS: str
    POSTAL_CODE: str
    ALT_NAMES: List[str]
