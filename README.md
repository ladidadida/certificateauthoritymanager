# CertificateAuthorityManager

CertificateAuthorityManager is an application to manage a bundle of a signing certificate together with signed certificates.
It is both, a library and a self running application and is intended to be used as certificate manager for local networks that want
to increase security by enabling HTTPS communication.

For me it is not yet clear, whether storing all certificates in one place is a good idea and maybe in next iterations I will remove the 
signed certificates storage completely.

One nice thing that works now is, that you can (re-)create a whole armada of signed certificates by providing a simple json formatted file.

## Usage

### As a standalone software

```
usage: main.py [-h] [--version] [--new] [--passphrase PASSPHRASE]
               [--fromjson FROMJSON] [--interactive] [--capubkey CAPUBKEY]
               [--serverprivkey SERVERPRIVKEY SERVERPRIVKEY]
               [--serverpubkey SERVERPUBKEY SERVERPUBKEY]
               database

positional arguments:
  database              Set the database file as reference.

optional arguments:
  -h, --help            show this help message and exit
  --version             show program's version number and exit
  --new                 Create a new database from scratch.
  --passphrase PASSPHRASE
  --fromjson FROMJSON   Create a new CA storage from config json file.
  --interactive         Start interactive session.
  --capubkey CAPUBKEY   Export CA public key.
  --serverprivkey SERVERPRIVKEY SERVERPRIVKEY
                        Export Server private key.
  --serverpubkey SERVERPUBKEY SERVERPUBKEY
                        Export Server public key.
```


#### DATABASE
The managed database that stores CA key and signed certificate keys.

Attention: In current state, databays is NOT ENCRYPTED!!! This will come in later version
or not at all if the manager part is removed.

#### PASSPHRASE
Passphrase used to encrypt/decrypt the stored DATABASE.

#### FROMJSON
A json file, that describes the CA information or certificate information used to create a new database or update one (depending on parmeters "--new")

The format of a json file is as follows:
```
{
    "owner_info" : {
        <LIST_OF_OIDS>,
        "ALT_NAMES" : {
            <LIST_OF_ALT_NAMES>
        }
    },
    "signed_certificates" : {
        <CERTIFICATE_IDENTIFIER_1> : {
            "owner_info" : {
                <LIST_OF_OIDS>,
                "ALT_NAMES" : {
                    <LIST_OF_ALT_NAMES>
                }
            }
        }
    }
}
```

With following possible fields:

##### <LIST_OF_OIDS>
Object identifier as used for certificate creation. See ... for a list of possible fields and their meanings.

Specify each OID setting by giving the OID name in upper letters, followed by a string:

`"COMMON_NAME" : "example.org"`

The field COMMON_NAME is mandatory.

##### <LIST_OF_ALT_NAMES>
Additionally to OIDS you can add alternative names as described here ...

##### Example

```json
{
  "owner_info" : {
    "COMMON_NAME" : "DUMMY CA"
  },
  "signed_certificates" : {
      "cert 1": {
        "owner_info": {
          "COMMON_NAME" : "example.org",
          "EMAIL_ADDRESS" : "admin@example.org"
        },
      },
      "cert 2": {
        "owner_info": {
          "COMMON_NAME" : "test.org",
          "EMAIL_ADDRESS" : "info@test.org",
          "ALT_NAMES" : [
            "www.test.org",
            "wb.test.org",
            "imap.test.org"
          ]
        },
      }
    }
}
```

### As a library

The CertificateAuthorityManager class implements both, the CertificateAuthority and the database for signed server
certificates.

## Thanks to

The project consists of a bunch of helper functions that are based on code examples
of https://realpython.com/python-https/#becoming-a-certificate-authority. For anybody that wants to learn Python or
extend python knowledge, realpython is a good start. Even for advanced options like SSL and HTTPS you can find some
useful info.
